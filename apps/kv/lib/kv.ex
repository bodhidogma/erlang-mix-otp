defmodule KV do
  use Application

  @moduledoc """
  Documentation for `KV`.
  """

  @impl true
  def start(_type, _args) do
    #    IO.puts("_kv.start()")
    KV.Supervisor.start_link(name: KV.Supervisor)
  end

  @impl true
  def stop(_state) do
    IO.puts("kv.stop()")
  end

  @doc """
  Hello world.

  ## Examples

      iex> KV.hello()
      :world

  """
  def hello do
    :world
  end
end
