defmodule KVServer.CommandTest do
  use ExUnit.Case, async: true
  doctest KVServer.Command

#  setup do
#    :kv_reg = KV.Registry
#  end

  test "create bucket" do
    assert KVServer.Command.run({:create, "ABucket"}, KV.Registry) == {:ok, "OK\r\n"}
  end

  test "put a key value" do
    KVServer.Command.run({:create, "ABucket"}, KV.Registry)
    assert KVServer.Command.run({:put, "ABucket", "AKey", "AVal"}, KV.Registry) == {:ok, "OK\r\n"}
  end

  test "get a key value" do
    KVServer.Command.run({:create, "ABucket"}, KV.Registry)
    KVServer.Command.run({:put, "ABucket", "AKey", "AVal"}, KV.Registry)
    assert KVServer.Command.run({:get, "ABucket", "AKey"}, KV.Registry) == {:ok, "AVal\r\nOK\r\n"}
  end

  test "delete a key" do
    KVServer.Command.run({:create, "ABucket"}, KV.Registry)
    KVServer.Command.run({:put, "ABucket", "AKey", "AVal"}, KV.Registry)
    assert KVServer.Command.run({:delete, "ABucket", "AKey"}, KV.Registry) == {:ok, "OK\r\n"}
  end
end
